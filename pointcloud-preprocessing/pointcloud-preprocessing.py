import numpy as np
import open3d as o3d
import pandas as pd
from numpy import genfromtxt
import argparse

# Define the command-line arguments
parser = argparse.ArgumentParser()
parser.add_argument('--file-path', type=str, help='Path to the file without extension')


def read_gocator_csv_and_convert_to_ascii(p_file_path):
    df = pd.read_csv(filepath_or_buffer=p_file_path + '.csv', skipinitialspace=True, header=None, skiprows=28,
                     low_memory=False)
    df.drop(df.tail(1).index, inplace=True)
    numpy_array = df.to_numpy(dtype=float)
    numpy_array = np.reshape(numpy_array, (int(numpy_array.size / 3), 3))
    df = pd.DataFrame(numpy_array, columns=['x', 'y', 'z'])
    df.dropna(how='all', inplace=True)
    df.to_csv(p_file_path + '.txt', header=None, index=None, sep=',', mode='w')
    print('Gocator CSV successfully to ASCII converted.')


def read_ascii_and_convert_to_pcd(p_file_path):
    pcd_np = genfromtxt(p_file_path + '.txt', delimiter=',')
    pcd = o3d.geometry.PointCloud()
    pcd.points = o3d.utility.Vector3dVector(pcd_np)
    o3d.io.write_point_cloud(p_file_path + '.pcd', pcd, write_ascii=True)
    print('ASCII successfully to PCD converted.')


def preprocess_pointcloud(p_file_path, voxel_size=0.5):
    pcd = o3d.io.read_point_cloud(p_file_path + '.pcd', remove_nan_points=True, remove_infinite_points=True)
    print('Import of Poincloud finished')
    pcd_downsampled = pcd.voxel_down_sample(voxel_size=voxel_size)
    print('Downsampling finished')
    pcd_downsampled.estimate_normals(search_param=o3d.geometry.KDTreeSearchParamHybrid(radius=0.1, max_nn=30))
    print('Normals calculated')
    pcd_noise_reduced, index = pcd_downsampled.remove_radius_outlier(nb_points=8, radius=2)
    o3d.io.write_point_cloud(p_file_path + '-processed.pcd', pcd_noise_reduced, write_ascii=True)


def main(file_path_without_extension):
    read_gocator_csv_and_convert_to_ascii(file_path_without_extension)
    read_ascii_and_convert_to_pcd(file_path_without_extension)
    preprocess_pointcloud(file_path_without_extension)


if __name__ == '__main__':
    # Parse the command-line arguments
    args = parser.parse_args()

    # Check if the file_path argument is provided
    if args.file_path:
        main(args.file_path.encode('unicode_escape').decode())
    else:
        # Set a default file path if the argument is not provided
        default_file_path = '../3D-files/replay_127534_2023-8-3_untouched'
        main(default_file_path)