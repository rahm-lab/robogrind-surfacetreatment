import numpy as np
import open3d as o3d
import pandas as pd
from coordinates import Coordinate

import argparse

# Define the command-line arguments
parser = argparse.ArgumentParser()
parser.add_argument('--reference-cloud', type=str, help='Path to the reference cloud')
parser.add_argument('--point-cloud', type=str, help='Path to the point cloud')
parser.add_argument('--export-point-cloud', type=str, help='Export path of the referenced point cloud')

def get_laser_height(p_ref_ring_coord, p_scan_start_coord, p_ref_ring_z1, p_ref_ring_z2, p_ref_ring_z3, p_ref_ring_z4):
    """
    Calculate laser height by using reference coordinates
    :param p_ref_ring_coord: TCP of robot, when it is on top of reference ring
    :param p_scan_start_coord: TCP of robot, when it is in the position where a scan starts
    :param p_ref_ring_z1: point on reference ring (top)
    :param p_ref_ring_z2: point on reference ring (right)
    :param p_ref_ring_z3: point on reference ring (bottom)
    :param p_ref_ring_z4: point on reference ring (left)
    :return: height of laser, z-value under TCP
    """
    z_mean = (p_ref_ring_z1 + p_ref_ring_z2 + p_ref_ring_z3 + p_ref_ring_z4) / 4
    return p_scan_start_coord.z - p_ref_ring_coord.z - z_mean


def get_grinder_height(p_ref_ring_coord, p_grinder_coord):
    """
    Calculate grinder height by using reference coordinates
    :param p_ref_ring_coord: TCP of robot, when it is on top of reference ring
    :param p_grinder_coord: TCP of robot, when grinder is on top of reference ring
    :return: height of grinder, z-value under TCP
    """
    return p_grinder_coord.z - p_ref_ring_coord.z


def get_laser_shift_x_y(p_laser_coord, p_ref_wall_coord):
    """
    Calculates shift of laser from robot TCP
    :param p_laser_coord: Coordinates of TCP, when laser line hits reference wall
    :param p_ref_wall_coord: Coordinates of TCP, when robot with reference tool is on reference wall
    :return: A shift vector with x and y
    """
    return Coordinate(x=p_laser_coord.x - p_ref_wall_coord.x, y=p_laser_coord.y - p_ref_wall_coord.y)


def extract_reference_point(p_point_cloud_df):
    """
    Extracts a reference point (x=0.xx) from the given point_cloud to calculate required transformations on point_cloud_df
    :param p_point_cloud_df: point cloud as pandas dataframe including reference scan with -/+x (incl. 0.xx), -/+ y-axis
    :return: reference_point right under scanner (orthogonal) with coordinates in aligned scanner system (y=0)
    """
    first_row_of_scan = p_point_cloud_df.query('y==y[0]')
    center_point_of_first_row = first_row_of_scan.iloc[(first_row_of_scan['x']).abs().argsort()[:1]]
    return Coordinate(x=center_point_of_first_row.x.item(), y=center_point_of_first_row.y.item(),
                      z=center_point_of_first_row.z.item())


def import_point_cloud_as_dataframe(p_path_to_point_cloud_file):
    """
    Imports a given point cloud in ASCII(txt) or PCD-format and processes it as a pandas dataframe
    :param p_path_to_point_cloud_file: Given point-cloud from scan in ascii format (x-, y-, z-values)
    :return: point cloud as pandas dataframe
    """
    if p_path_to_point_cloud_file.endswith('.txt'):
        point_cloud = pd.read_csv(p_path_to_point_cloud_file, header=None, sep=',')
        point_cloud.columns = ["x", "y", "z"]
        print("Pointcloud from ASCII(txt) successful loaded")
    elif p_path_to_point_cloud_file.endswith('.pcd'):
        pcd_load = o3d.io.read_point_cloud(p_path_to_point_cloud_file)
        point_cloud_np_array = np.asarray(pcd_load.points)
        point_cloud = pd.DataFrame(point_cloud_np_array, columns=["x", "y", "z"])
        print("Pointcloud from PCD successful loaded")
    else:
        point_cloud = pd.DataFrame([])
        print("Import failed. File format not supported.")
    return point_cloud.astype('float32')


def extract_transformation_vector(p_scan_reference_start_coord, p_scan_start_coord, p_laser_shift_x_y, p_laser_height,
                                  p_reference_pointcloud_df):
    """
    Extracts a transformation vector from the reference point cloud to the point cloud by using the coordinates of reference points
    :param p_scan_reference_start_coord: TCP of robot, when it is in the position where the reference scan starts
    :param p_scan_start_coord: TCP of robot, when it is in the position where the point cloud scan starts
    :param p_laser_shift_x_y: shift of laser from robot TCP in x and y direction
    :param p_laser_height: height of laser, z-value under TCP
    :param p_reference_pointcloud_df: Given point-cloud from scan as Pandas dataframe
    :return: Coordinates of transformation vector
    """
    reference_point_in_cloud = extract_reference_point(p_reference_pointcloud_df)
    print("Reference point in point-cloud: ", reference_point_in_cloud)
    laser_line_coord = Coordinate((p_scan_reference_start_coord - Coordinate(p_laser_shift_x_y, z=0)),
                                  z=p_scan_reference_start_coord.z - p_laser_height - reference_point_in_cloud.z)
    print("Coordinates of laser line: ", laser_line_coord)
    extracted_transformation_vector = reference_point_in_cloud - laser_line_coord - (
            p_scan_start_coord - p_scan_reference_start_coord)
    print("Values of transformation_vector: ", extracted_transformation_vector)
    return extracted_transformation_vector


def transform_scan_to_robot_coordinates(p_transformation_vector, p_point_cloud_df):
    """
    Transform a given point-cloud as pandas dataframe to the coordinate system of the robot
    :param p_transformation_vector: Vector from referencing to transform a point cloud's coordinates into the robot's coordinate system
    :param p_point_cloud_df: Given point-cloud from scan as pandas dataframe
    :return: void
    """
    p_point_cloud_df['x'] = p_point_cloud_df['x'] - p_transformation_vector.x
    p_point_cloud_df['y'] = p_point_cloud_df['y'] - p_transformation_vector.y
    p_point_cloud_df['z'] = p_point_cloud_df['z'] - p_transformation_vector.z
    return p_point_cloud_df


def export_point_cloud_df(p_point_cloud_df, p_export_path):
    """
    Exports point cloud from dataframe to specified export path and format
    :param p_point_cloud_df: Point cloud as Pandas dataframe
    :param p_export_path: Export path for transformed point-cloud in ASCII(txt) or PCD format
    :return:
    """
    if p_export_path.endswith('.txt'):
        p_point_cloud_df.to_csv(p_export_path, header=None, index=None, sep=',', mode='w')
        print("Export of transformed point-cloud was successful.")
    elif p_export_path.endswith('.pcd'):
        point_cloud_np_array = p_point_cloud_df.to_numpy()
        pcd_export = o3d.geometry.PointCloud()
        pcd_export.points = o3d.utility.Vector3dVector(point_cloud_np_array)
        o3d.io.write_point_cloud(p_export_path, pcd_export, write_ascii=True)
        print("Export of transformed point-cloud was successful.")
    else:
        print('Export-Format not supported')


def align_point_cloud_based_on_scan_orientation(p_point_cloud_df, scan_orientation):
    """
    Aligns the given point cloud as Pandas dataframe to robot coordinate system orientation as preparation for transformation,
    starting with an alignment to y=0
    :param p_point_cloud_df: Point cloud as Pandas dataframe in scan coordinate system
    :param scan_orientation: Orientation of scan in roboter coordinate system
    :return: scan in robot coordinate system direction starting with y=0
    """
    y_difference = p_point_cloud_df.min().y
    alignment_vector = Coordinate(x=0, y=y_difference, z=0)

    p_point_cloud_df['y'] = p_point_cloud_df['y'] - alignment_vector.y
    print('Point cloud aligned to y=0 coordinate system')

    turn_degree = 0
    if scan_orientation == 'y':
        turn_degree = 0
    elif scan_orientation == '-x':
        turn_degree = 90
    elif scan_orientation == '-y':
        turn_degree = 180
    elif scan_orientation == 'x':
        turn_degree = 270
    theta = np.radians(turn_degree)
    cosinus, sinus = np.cos(theta), np.sin(theta)

    cosinus = int(cosinus)
    sinus = int(sinus)

    p_point_cloud_df_x = p_point_cloud_df['x']
    p_point_cloud_df_y = p_point_cloud_df['y']

    p_point_cloud_df['x'] = p_point_cloud_df_x * cosinus - p_point_cloud_df_y * sinus
    p_point_cloud_df['y'] = p_point_cloud_df_x * sinus + p_point_cloud_df_y * cosinus

    return p_point_cloud_df


def validate_referencing(p_point_cloud_source, p_point_cloud_target, p_export_path):
    """
    Validates the referencing algorithm by checking point cloud distance between two point clouds
    :param p_point_cloud_source: source point cloud for validation
    :param p_point_cloud_target: target point cloud for validation
    :param p_export_path: export path for distance dataframe
    :return: none
    """
    dists = p_point_cloud_source.compute_point_cloud_distance(p_point_cloud_target)
    dists = np.asarray(dists)

    np.savetxt(p_export_path + '.txt', dists)
    print("Export of distance array was successful.")


def main(p_reference_cloud_path, p_point_cloud_path, p_export_point_cloud_path):
    # Coordinates of robot, when TCP on top of reference ring
    ref_ring_coord = Coordinate(x=730, y=290, z=11.7)
    # Coordinates of robot, when TCP on reference wall (reference point)
    ref_wall_coord = Coordinate(x=721.20, y=384.48, z=58.33)
    # Coordinates of robot in scan-reference-start-position
    scan_reference_start_coord = Coordinate(x=730, y=290, z=815)
    # z-values on reference ring, taken from scan-file
    ref_ring_z1 = 401.637
    ref_ring_z2 = 400.066
    ref_ring_z3 = 400.142
    ref_ring_z4 = 400.035
    # Coordinates of robot, when laser line hits reference wall (scan-x=0 on reference point)
    laser_reference_wall_coord = Coordinate(x=730, y=287, z=815)
    # Coordinates of robot in scan-start-position of point cloud
    scan_start_coord = Coordinate(x=730, y=290, z=250)
    # Coordinates of robot, when grinder is on top of reference ring
    grinder_coord = Coordinate(x=716.20, y=329.85, z=171.58)
    # Set default order of class 'Coordinate' from module 'coordinates'
    Coordinate.default_order = 'xyz'
    laser_shift_x_y = get_laser_shift_x_y(laser_reference_wall_coord, ref_wall_coord)
    laser_height = get_laser_height(ref_ring_coord, scan_reference_start_coord, ref_ring_z1, ref_ring_z2, ref_ring_z3,
                                    ref_ring_z4)
    reference_cloud_df = import_point_cloud_as_dataframe(p_reference_cloud_path)
    point_cloud_df = import_point_cloud_as_dataframe(p_point_cloud_path)
    reference_cloud_df = align_point_cloud_based_on_scan_orientation(reference_cloud_df, '-y')
    point_cloud_df = align_point_cloud_based_on_scan_orientation(point_cloud_df, '-y')
    transformation_vector = extract_transformation_vector(scan_reference_start_coord, scan_start_coord, laser_shift_x_y,
                                                          laser_height, reference_cloud_df)
    transformed_cloud_df = transform_scan_to_robot_coordinates(transformation_vector, point_cloud_df)
    export_point_cloud_df(transformed_cloud_df, p_export_point_cloud_path)
    print("Laser height: ",
          get_laser_height(ref_ring_coord, scan_reference_start_coord, ref_ring_z1, ref_ring_z2, ref_ring_z3,
                           ref_ring_z4))
    print("Grinder height: ", get_grinder_height(ref_ring_coord, grinder_coord))
    # point_cloud_source = o3d.io.read_point_cloud("../3D-files/replay_127534_2023-5-5-processed-transformed.pcd")
    # point_cloud_target = o3d.io.read_point_cloud("../3D-files/replay_127534_2023-5-5 (4)-processed-transformed.pcd")
    # validate_referencing(point_cloud_source, point_cloud_target, 'distance-replay_127534_2023-5-5-processed-transformed-(4)')


if __name__ == '__main__':
    # Parse the command-line arguments
    args = parser.parse_args()

    if args.reference_cloud and args.point_cloud and args.export_point_cloud:
        main(args.reference_cloud.encode('unicode_escape').decode(), args.point_cloud.encode('unicode_escape').decode(), args.export_point_cloud.encode('unicode_escape').decode())
    else:
        default_reference_cloud_path = '../3D-files/2023-07-04-ref_cloud.txt'
        default_point_cloud_path = '../3D-files/replay_127534_2023-7-20 (3)-processed.pcd'
        default_export_point_cloud_path = '../3D-files/replay_127534_2023-7-20 (3)-processed-transformed.pcd'
        main(default_reference_cloud_path, default_point_cloud_path, default_export_point_cloud_path)