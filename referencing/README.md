# Referencing Documentation
![Image of reference tool](IMG_20221128_161712.jpg)
Follow these steps and extract robot's TCP in reference to base for each:
1. Reference TCP
    1. Set TCP on top of reference ring
    2. Set TCP with point tool on point of reference wall
2. Reference scanner
    1. Move to scanner home position
    2. Scan reference ring, extract z1-z4 values from reference ring
       (z1=12 o'clock, z2=3 o'clock, z3=6 o'clock, z4=9 o'clock) and export scan as csv
    3. Move to position where laser hits reference point (scan-x=0 is on position of point tool of step 1.2)
3. Reference grinder
    1. Move grinder on top of reference ring

Insert values in algorithm and calculate laser height, grinder height and transform a
point cloud into the robot's coordinate system.