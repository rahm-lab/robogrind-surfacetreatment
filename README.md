# RoboGrind
This repository contains the code for the "3D surface scanning" component of the ICRA 2024 paper "RoboGrind: Intuitive and Interactive Surface Treatment with Industrial
Robots".

The project structure is based on the [pipeline](#Pipeline). 
Some of the folders contain additional README files.
Some of the subcomponents are based on C++ and some on Python.

## Pipeline
1. Referencing laser line scanner and grinder in robot coordinate system
2. Scan of object
3. Preprocessing of point clouds
4. Registration of point clouds to complete 3D-model (*not included in the repository*)
5. Transformation of 3D-model into robot coordinate system based on referencing 
6. Detection of defects
7. Calculation of grinding trajectory (*not included in the repository*)
7. Grinding (*not included in the repository*)

## Setup
### Infrastructure
Our parts are mentioned in brackets:
- Robot (UR5e or UR10e)
- Laser line scanner (LMI Gocator 2490)
- Sander (OnRobot Sander)
- Computer (Windows 11 with WSL 2 and Ubuntu 20.04 6 LTS distribution)
- Create and configure a local network (LAN) of robot, scanner, grinder and computer
### Computer
- Setup conda/python environment with environment.yml
- Create 3D-files folder inside project folder for raw point clouds from laser line scanner
- Install ROS and [Universal Robots ROS Driver](https://github.com/UniversalRobots/Universal_Robots_ROS_Driver)
- Start Gocator web application to export point clouds
- Replace paths in project's python scripts to match you point cloud paths
### Robot
- Install URCaps for Gocator and Sander