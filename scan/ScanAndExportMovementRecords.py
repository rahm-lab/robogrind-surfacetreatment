import rospy

import tf2_ros

import csv
from datetime import datetime

from multiprocessing import Process

from std_srvs.srv import Trigger
from ur_dashboard_msgs.srv import Load, GetProgramState, GetRobotMode


def init_services():
    global brake_release, load_program, play, get_robot_mode, program_state
    rospy.wait_for_service('/ur_hardware_interface/dashboard/get_robot_mode')
    rospy.wait_for_service('/ur_hardware_interface/dashboard/program_state')
    get_robot_mode = rospy.ServiceProxy('/ur_hardware_interface/dashboard/get_robot_mode', GetRobotMode)
    program_state = rospy.ServiceProxy('/ur_hardware_interface/dashboard/program_state', GetProgramState)

    brake_release = rospy.ServiceProxy('/ur_hardware_interface/dashboard/brake_release', Trigger)
    load_program = rospy.ServiceProxy('/ur_hardware_interface/dashboard/load_program', Load)
    rospy.wait_for_service('/ur_hardware_interface/dashboard/play')
    play = rospy.ServiceProxy('/ur_hardware_interface/dashboard/play', Trigger)


def init_tf_listener():
    global tfBuffer, listener
    rospy.init_node('tf2_listener', anonymous=True)

    tfBuffer = tf2_ros.Buffer()
    listener = tf2_ros.TransformListener(tfBuffer)


def startup_and_release_brake():
    rospy.wait_for_service('/ur_hardware_interface/dashboard/brake_release')
    try:
        resp_brake_release = brake_release()
        rospy.loginfo(resp_brake_release)
    except rospy.ServiceException as exc:
        print("Service did not process request: " + str(exc))

    rospy.loginfo(get_robot_mode())
    while not rospy.is_shutdown() and not get_robot_mode().robot_mode.mode == 7:
        rate.sleep()

    rospy.loginfo('Robot is running')


def wait_until_ur_program_is_playing_and_stopped():
    rospy.loginfo(program_state())
    while not rospy.is_shutdown() and not program_state().state.state == 'PLAYING':
        rate.sleep()
    rospy.loginfo(program_state())
    while not rospy.is_shutdown() and not program_state().state.state == 'STOPPED':
        rate.sleep()
    rospy.loginfo(program_state())


def load_and_run_program_on_ur(program_name, program_path, timeout=30):
    rospy.wait_for_service('/ur_hardware_interface/dashboard/load_program')
    try:
        load_program(program_path + program_name + '.urp')
    except rospy.ServiceException as exc:
        print("Service did not process request: " + str(exc))
    rospy.wait_for_service('/ur_hardware_interface/dashboard/play')
    try:
        play()
    except rospy.ServiceException as exc:
        print("Service did not process request: " + str(exc))

    p1 = Process(target=wait_until_ur_program_is_playing_and_stopped,
                 name='wait_until_ur_program_is_playing_and_stopped')
    p1.start()
    p1.join(timeout=timeout)
    p1.terminate()
    if p1.exitcode is None:
        print(f'{p1} timed out!')
    if p1.exitcode == 0:
        print(f'{p1} finished in time')


def move_to(position, program_path):
    load_and_run_program_on_ur(position, program_path, 30)


def record_transformation():
    if not rospy.is_shutdown():
        try:
            trans = tfBuffer.lookup_transform('base', 'tool0_controller', rospy.Time())
            writer.writerow(
                [trans.transform.translation.x, trans.transform.translation.y, trans.transform.translation.z,
                 trans.transform.rotation.x, trans.transform.rotation.y, trans.transform.rotation.z,
                 trans.transform.rotation.w])
        except (tf2_ros.LookupException, tf2_ros.ConnectivityException, tf2_ros.ExtrapolationException):
            print("Transformation could not be looked up")


if __name__ == '__main__':
    PROGRAM_PATH = 'robogrind/'
    SCAN_ITERATIONS = 1

    init_services()
    init_tf_listener()

    global rate
    rate = rospy.Rate(10.0)

    # Start-Up UR including Brake-Release
    startup_and_release_brake()

    # Execute InitAndCalibration-Program for Gocator 2490 on UR
    # load_and_run_program_on_ur('calib', PROGRAM_PATH, 120)
    load_and_run_program_on_ur('init', PROGRAM_PATH, 30)

    # Prepare CSV-File for Movement-Recording
    global writer
    now = datetime.now()
    date_time = now.strftime("%Y-%m-%d-%H-%M-%S")
    movement_record_file = open(date_time + '-image-registration-positions.csv', 'w')
    writer = csv.writer(movement_record_file)
    writer.writerow(['transX', 'transY', 'transZ', 'rotX', 'rotY', 'rotZ', 'rotW'])

    # Move to Home Position (0.0)
    load_and_run_program_on_ur('home_position_versuch', PROGRAM_PATH, 5)

    # Record Home Position (0.0)
    rospy.loginfo('Recording Home Position(0.0)')
    record_transformation()

    # Show Popup to place items for scan on UR-Tablet
    load_and_run_program_on_ur('place_items_popup', PROGRAM_PATH, 300)

    # Execute Scan-Program for Gocator 2490 on UR
    load_and_run_program_on_ur('scan', PROGRAM_PATH, 60)

    # Record End-Position (0.1)
    rospy.loginfo('Recording End Position(0.1)')
    record_transformation()

    # Loop until all SCAN_ITERATIONS are done
    for x in range(1, SCAN_ITERATIONS):
        # Move to Start-Position (x.0)
        move_to('position_' + str(x) + '_0', PROGRAM_PATH)

        # Record Start-Position (x.0)
        rospy.loginfo('Recording Start Position(' + str(x) + '.0)')
        record_transformation()

        # Execute Scan-Program for Gocator 2490 on UR
        load_and_run_program_on_ur('scan', PROGRAM_PATH)

        # Record End-Position (1.1)
        rospy.loginfo('Recording End Position(' + str(x) + '.1)')
        record_transformation()

    # Close CSV-File with Movement-Records
    movement_record_file.close()
