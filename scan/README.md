# Scan Documentation
## Getting started
In order to run the `ScanAndExportMovementRecords`-Script
you need to set up the UR-Robot following these steps:
1. Connect the UR robot, the Gocator scanner and a PC to a network
2. Install URCap for Gocator
3. Setup URCap for Gocator on UR
4. Start GoAccelerator on the PC
5. Calibrate the Gocator sensor
   1. Create a calib Job on the Gocator sensor (Exp. `scan/GoRobotCalib.job`)
   2. Create a calib program on the UR robot (Exp. `ur10e/calib.urp`)
      - Move to home position
      - Gocator Calibrate
   3. Create an init program on the UR robot (Exp. `ur10e/init.urp`)
      - Gocator Connect
      - Gocator Load Job (`GoRobotScan<UR_Type>`)
6. Scan
   1. Create a scan Job on the Gocator sensor (Exp. `scan/GoRobotScanUR10e.job`)
   2. Create a place_items_popup on the UR robot (Exp. `ur10e/place_items_popup.urp`)
   3. Create scan position files for the start of scans (`position_X_0.urp`)
7. Start ROS and UR Robot Driver with robot profile `roslaunch ur_robot_driver ur<UR_Number>_bringup.launch robot_ip:=<Robot_IP> kinematics_config:=<PATH_to_Calibration_YAML>`

## Troubleshooting
### Scan movement is diagonally 
Adapt the TCP-position of the UR (Exp. TCP profile `Gocator` inside of `ur10e/default.installation`)
