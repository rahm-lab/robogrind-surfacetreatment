#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <boost/program_options.hpp>
#include <pcl/common/common.h>
#include <pcl/io/pcd_io.h>
#include <Eigen/Dense>
#include <Eigen/QR>
#include <iostream>
#include <chrono>

using namespace boost;
using namespace std;
using namespace pcl;
namespace po = boost::program_options;

// A helper function to simplify the main part.
template<class T>
ostream& operator<<(ostream& os, const vector<T>& v)
{
    copy(v.begin(), v.end(), ostream_iterator<T>(os, " "));
    return os;
}

// Store the extracted x-vectors and z-vectors
struct Vectors {
    vector<vector<double>> xvectors;
    vector<vector<double>> zvectors;
};

// Store all points of the same y-value in one vector. Each vector is then stored in pointvectors.
Vectors createSegments(PointCloud<PointXYZ>::Ptr cloud);

// Estimate the polynomial for the input dataset
void polyfit(vector<vector<double>> &xvecs,
		     vector<vector<double>> &zvecs,
		     vector<vector<double>> &coeffs,
		     int order);

// Calculate the values using the estimated polynomial
vector<vector<double>> estValues(vector<vector<double>> polyCoefs, vector<vector<double>> xvectors);

// Calculate the distance between the points of each original dataset and polynomial-estimated dataset along with the averages
vector<double> getDist(vector<vector<double>> zvectors, vector<vector<double>> est_zvectors);

// Calculate the roughness parameter of the surface
double getRoughness(vector<double> dists);

/*
// View the input cloud
void cloudsViewer(PointCloud<PointXYZ>::Ptr &cloud, bool spin = false);
void cloudsViewer(PointCloud<PointXYZ>::Ptr &cloud, PointCloud<PointXYZ>::Ptr &cloud2);
*/

int main(int ac, char* av[])
{
	std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
    
    // Get current directory path
    char dir[256];
    getcwd(dir, 256);

	// Declare supported program_options
	std::string file_path;
	
	po::options_description hidden("Hidden options");
	hidden.add_options()
    	("input-file", po::value< std::string >(&file_path), "input file")
    ;     

	po::options_description cmdline_options;
        cmdline_options.add(hidden);

	po::positional_options_description p;
	p.add("input-file", -1);

	po::variables_map vm;
	po::store(po::command_line_parser(ac, av).
          options(cmdline_options).positional(p).run(), vm);
	po::notify(vm);

	if (vm.count("help")) {
		cout << "Usage: statistica_removal [file_path]\n";
    	return 0;
	}

	// Load the original pcd to a point cloud pointer
    PointCloud<PointXYZ>::Ptr cloud (new PointCloud<PointXYZ>);
	PCDReader reader;
	reader.read<PointXYZ>(file_path, *cloud);

    // Sort point cloud in y-ascending order
    std::sort(cloud->begin(), cloud->end(), [](const PointXYZ& p1, const PointXYZ& p2) {
        return p1.y < p2.y;
    });
    
    // Extract the x-z segments from the cloud
    Vectors xzvectors;
    xzvectors = createSegments(cloud); // set to true to visualize splines iteratively

    // Apply the ploynomial fit on each segment
    vector<vector<double>> polyCoefs;
    int order = 2;
    polyfit(xzvectors.xvectors, xzvectors.zvectors, polyCoefs, order);

    // Calculate the z-values using the polynomial and the distance between each value and its original correspondence 
    vector<vector<double>> est_zvectors = estValues(polyCoefs, xzvectors.xvectors);
    vector<double> abs_distances = getDist(xzvectors.zvectors, est_zvectors);

    double r_a = getRoughness(abs_distances);

	std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
	std::cout << "\nRun time = " << std::chrono::duration_cast<std::chrono::seconds>(end - begin).count() << " s." << std::endl;
    return 0;
}

Vectors createSegments(PointCloud<PointXYZ>::Ptr cloud) {
    Vectors rvecs;
    // Declare assisting variables
    float y0 = cloud->points[0].y;
    vector<PointXYZ> pv;
    vector<double> xvalues, zvalues;
    vector<vector<PointXYZ>> pointvectors;
    vector<vector<double>> xvectors, zvectors;
    int vec_size = 0;
    int vec_num = 0;
    size_t extracted_points = 0;

    for(const auto& point : *cloud) {    
        if(point.y == y0) {
            pv.push_back(point);
            xvalues.push_back(point.x);
            zvalues.push_back(point.z);
            extracted_points++;
        }
        else {
            pointvectors.push_back(pv);
            xvectors.push_back(xvalues);
            zvectors.push_back(zvalues);
            vec_size += pv.size();
            vec_num++;
            extracted_points++;
            y0 = point.y;
            pv.clear();
            xvalues.clear();
            zvalues.clear();
            pv.push_back(point);
            xvalues.push_back(point.x);
            zvalues.push_back(point.z);
        }
    }

    // Process the last segment of points
    pointvectors.push_back(pv);
    xvectors.push_back(xvalues);
    zvectors.push_back(zvalues);
    vec_size += pv.size();
    vec_num++;
    extracted_points++;

    // print processig element features
    float avgsize = vec_size / vec_num;
    printf("The cloud has %lu points.\n", cloud->points.size());
    printf("The program has extracted %lu points.\n", extracted_points);
    printf("The extracted points are split into %lu z-parts, and %lu x-parts.\n", zvectors.size(), xvectors.size());
    printf("Each part has, on average, %f elements\n\n", avgsize);

    rvecs.xvectors = xvectors;
    rvecs.zvectors = zvectors;
    return rvecs;
}

void polyfit(vector<vector<double>> &xvecs,
		     vector<vector<double>> &zvecs,
		     vector<vector<double>> &coeffs,
		     int order) {
    vector<double> coeff;

    for(int i = 0; i < xvecs.size(); i++) {
        if(!(xvecs[i].size() >= order + 1)) {
            xvecs.erase(xvecs.begin() + i);
            zvecs.erase(zvecs.begin() + i);
            i--;
            continue;
        }

        // Create Matrix Placeholder of size n x k, n= number of datapoints, k = order of polynomial, for exame k = 3 for cubic polynomial
        Eigen::MatrixXd T(xvecs[i].size(), order + 1);
        Eigen::VectorXd V = Eigen::VectorXd::Map(&zvecs[i].front(), zvecs[i].size());
        Eigen::VectorXd result;

        // check to make sure inputs are correct
        assert(xvecs[i].size() == zvecs[i].size());
        assert(xvecs[i].size() >= order + 1);

        // Populate the matrix
        for(size_t j = 0 ; j < xvecs[i].size(); ++j)
        {
            for(size_t k = 0; k < order + 1; ++k)
            {
                T(j, k) = pow(xvecs[i].at(j), k);
            }
        }
        
        // Solve for linear least square fit
        result  = T.householderQr().solve(V);
        coeff.resize(order+1);
        for (int n = 0; n < order+1; n++)
        {
            coeff[n] = result[n];
        }

        std::reverse(coeff.begin(), coeff.end());
        coeffs.push_back(coeff);
        coeff.clear();
    }
}

vector<vector<double>> estValues(vector<vector<double>> polyCoefs, vector<vector<double>> xvectors) {
    int vcounter(0);
    double zEst;
    vector<double> estZVec;
    vector<vector<double>> est_zvectors;
    for(vector<double> vector : xvectors) {
        for(double x : vector) {
            //zEst = ((pow(x,3))*polyCoefs[vcounter][0]) + ((pow(x,2))*polyCoefs[vcounter][1]) + (x*polyCoefs[vcounter][2]) + polyCoefs[vcounter][3]; // 3rd degree poly
            zEst = ((pow(x,2))*polyCoefs[vcounter][0]) + (x*polyCoefs[vcounter][1]) + polyCoefs[vcounter][2]; // 2nd degree poly
            //zEst =  (x*polyCoefs[vcounter][0]) + polyCoefs[vcounter][1]; // 1st degree poly
            estZVec.push_back(zEst);
        }
        est_zvectors.push_back(estZVec);
        estZVec.clear();
        vcounter++;
    }
    return est_zvectors;
}

vector<double> getDist(vector<vector<double>> zvectors, vector<vector<double>> est_zvectors) {
    double dist;
    vector<double> distances;

    for(int i = 0; i < zvectors.size(); i++) {
        for(int j = 0; j < zvectors[i].size(); j++) {
            dist = 1000 * abs(zvectors[i][j] - est_zvectors[i][j]); // in micrometer
            distances.push_back(dist);
        }
    }

    return distances;
}

double getRoughness(vector<double> dists) {
    double total;
    for(double dist : dists) {
        total += dist;
    }
    cout << "Total is: " << total << " micrometers." << endl;
    double ra = total / dists.size();
    cout << "R_a is: " << ra << " micrometers." << endl;
    return ra;
}