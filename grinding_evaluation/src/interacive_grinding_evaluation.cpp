#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/filters/passthrough.h>
#include <pcl/filters/crop_box.h>
#include <pcl/common/common.h> 
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <iostream>
#include <thread>

using namespace std::chrono_literals;
using namespace std;
using namespace pcl;

PointCloud<PointXYZ>::Ptr cloud (new PointCloud<PointXYZ>);

class BoundingBox {
public:
    void initCube() {
        getMinMax3D(*cloud, min, max);
        yDist = max.y - min.y;
        xDist = max.x - min.x;
        zDist = max.z - min.z;

        p1.y = max.y; p1.x = min.x+(xDist*0.25); p1.z = max.z;
        p2.y = max.y; p2.x = min.x; p2.z = max.z;
        p3.y = min.y+(yDist*0.75); p3.x = min.x; p3.z = max.z;
        p4.y = min.y+(yDist*0.75); p4.x = min.x+(xDist*0.25); p4.z = max.z;

        p5.y = max.y; p5.x = min.x+(xDist*0.25); p5.z = min.z;
        p6.y = max.y; p6.x = min.x; p6.z = min.z;
        p7.y = min.y+(yDist*0.75); p7.x = min.x; p7.z = min.z;
        p8.y = min.y+(yDist*0.75); p8.x = min.x+(xDist*0.25); p8.z = min.z;
    }

    void moveBoxLeft() {
        if (p2.x == min.x) {}
        else {
            p1.x -= (xDist*0.25);
            p2.x -= (xDist*0.25);
            p3.x -= (xDist*0.25);
            p4.x -= (xDist*0.25);
            p5.x -= (xDist*0.25);
            p6.x -= (xDist*0.25);
            p7.x -= (xDist*0.25);
            p8.x -= (xDist*0.25);
        }
    }

    void moveBoxRight() {
        if (p1.x == max.x) {}
        else {
            p1.x += (xDist*0.25);
            p2.x += (xDist*0.25);
            p3.x += (xDist*0.25);
            p4.x += (xDist*0.25);
            p5.x += (xDist*0.25);
            p6.x += (xDist*0.25);
            p7.x += (xDist*0.25);
            p8.x += (xDist*0.25);
        }
    }

    void moveBoxUp() {
        if (p1.y == max.y) {}
        else {
            p1.y += (yDist*0.25);
            p2.y += (yDist*0.25);
            p3.y += (yDist*0.25);
            p4.y += (yDist*0.25);
            p5.y += (yDist*0.25);
            p6.y += (yDist*0.25);
            p7.y += (yDist*0.25);
            p8.y += (yDist*0.25);
        }
    }

    void moveBoxDown() {
        if (p3.y == min.y) {}
        else {
            p1.y -= (yDist*0.25);
            p2.y -= (yDist*0.25);
            p3.y -= (yDist*0.25);
            p4.y -= (yDist*0.25);
            p5.y -= (yDist*0.25);
            p6.y -= (yDist*0.25);
            p7.y -= (yDist*0.25);
            p8.y -= (yDist*0.25);
        }
    }

    void cropCloud() {
        CropBox<PointXYZ> cb;
        Eigen::Vector4f minP (p7.x, p7.y, p7.z, 1.0);
        Eigen::Vector4f maxP (p1.x, p1.y, p1.z, 1.0);
        cb.setInputCloud(cloud);
        cb.setMin(minP);
        cb.setMax(maxP);
        cb.filter(cropped_cloud);
        getMinMax3D(cropped_cloud, cmin, cmax);
    }

    void createSegments() {
        y0 = cropped_cloud.points[0].y;
        pv.clear();
        xvalues.clear();
        zvalues.clear();
        extracted_points = 0;
        pointvectors.clear();
        xvectors.clear();
        zvectors.clear();
        vec_size = 0;
        vec_num = 0;
        // sort point cloud in y-ascending order
        std::sort(cropped_cloud.begin(), cropped_cloud.end(), [](const PointXYZ& point1, const PointXYZ& point2) {
            return point1.y < point2.y;
        });

        for(const auto& point : cropped_cloud) {    
            if(point.y == y0) {
                pv.push_back(point);
                xvalues.push_back(point.x);
                zvalues.push_back(point.z);
                extracted_points++;
            }
            else {
                pointvectors.push_back(pv);
                xvectors.push_back(xvalues);
                zvectors.push_back(zvalues);
                vec_size += pv.size();
                vec_num++;
                extracted_points++;
                y0 = point.y;
                pv.clear();
                xvalues.clear();
                zvalues.clear();
                pv.push_back(point);
                xvalues.push_back(point.x);
                zvalues.push_back(point.z);
            }
        }

        // Process the last segment of points
        pointvectors.push_back(pv);
        xvectors.push_back(xvalues);
        zvectors.push_back(zvalues);
        vec_size += pv.size();
        vec_num++;

        // print processig element features
        avgsize = vec_size / vec_num;
    }

    void polyFit() {
        polyCoefs.clear();
        for(int i = 0; i < xvectors.size(); i++) {
            if(!(xvectors[i].size() >= order + 1)) {
                xvectors.erase(xvectors.begin() + i);
                zvectors.erase(zvectors.begin() + i);
                i--;
                continue;
            }

        // Create Matrix Placeholder of size n x k, n= number of datapoints, k = order of polynomial, for exame k = 3 for cubic polynomial
        Eigen::MatrixXd T(xvectors[i].size(), order + 1);
        Eigen::VectorXd V = Eigen::VectorXd::Map(&zvectors[i].front(), zvectors[i].size());
        Eigen::VectorXd result;

        // check to make sure inputs are correct
        assert(xvectors[i].size() == zvectors[i].size());
        assert(xvectors[i].size() >= order + 1);

        // Populate the matrix
        for(size_t j = 0 ; j < xvectors[i].size(); ++j)
        {
            for(size_t k = 0; k < order + 1; ++k)
            {
                T(j, k) = pow(xvectors[i].at(j), k);
            }
        }
        
        // Solve for linear least square fit
        result  = T.householderQr().solve(V);
        coeff.resize(order+1);
        for (int n = 0; n < order+1; n++)
        {
            coeff[n] = result[n];
        }

        std::reverse(coeff.begin(), coeff.end());
        polyCoefs.push_back(coeff);
        coeff.clear();
        }
    }

    void estValues() {
        int vcounter(0);
        double zEst(0);
        estZVec.clear();
        est_zvectors.clear();

        for(vector<double> vector : xvectors) {
            for(double x : vector) {
                zEst = ((pow(x,2))*polyCoefs[vcounter][0]) + (x*polyCoefs[vcounter][1]) + polyCoefs[vcounter][2]; // 2nd degree poly
                estZVec.push_back(zEst);
            }
            est_zvectors.push_back(estZVec);
            estZVec.clear();
            vcounter++;
        }
    }

    void getDist() {
        double dist(0);
        distances.clear();

        for(int i = 0; i < zvectors.size(); i++) {
            for(int j = 0; j < zvectors[i].size(); j++) {
                dist = 1000 * abs(zvectors[i][j] - est_zvectors[i][j]); // in micrometer
                distances.push_back(dist);
            }
        }
    }

    void getRauheit() {
        total = 0;
        r_a = 0;
        for(double dist : distances) {
            total += dist;
        }
        r_a = total / distances.size();
    }

    void printResults() {
        printf("The cropped cloud has %lu points.\n", cropped_cloud.size());
        printf("The program has extracted %lu points.\n", extracted_points);
        printf("The minimum z-value is: %f.\nThe maximum z-value is: %f.\n", cmin.z, cmax.z);
        printf("The extracted points are split into %lu z-parts, and %lu x-parts.\n", zvectors.size(), xvectors.size());
        printf("Each part has, on average, %f elements\n\n", avgsize);
        printf("Total is: %f micrometers.\n", total);
        printf("R_a is: %f micrometers.\n\n", r_a);
        if(save_file) {
            string crop_path = file_path;
            crop_path.insert(crop_path.size()-4, ("_crop"+to_string(crop_num)));
            pcl::io::savePCDFileASCII (crop_path, cropped_cloud);
            printf("file saved as: %s\n", crop_path.c_str());
            crop_num++;
        }
    }     

    // cropping variables
    double r_a, total;
    PointCloud<PointXYZ> cropped_cloud;
    PointXYZ p1, p2, p3, p4, p5, p6, p7, p8;
    PointXYZ min, max, cmin, cmax;
    double xDist, yDist, zDist;

    // segmentation variables
    float avgsize;
    float y0;
    vector<PointXYZ> pv;
    vector<double> xvalues, zvalues;
    vector<vector<PointXYZ>> pointvectors;
    vector<vector<double>> xvectors, zvectors;
    int vec_size = 0;
    int vec_num = 0;
    size_t extracted_points = 0;

    // polynomial fitting variables
    vector<vector<double>> polyCoefs;
    const int order = 2;
    vector<double> coeff;

    vector<double> estZVec;
    vector<vector<double>> est_zvectors;
    vector<double> distances;

    bool save_file = false;
    int crop_num = 1;
    string file_path;
};

BoundingBox bb;

void keyboardEventOccurred(const pcl::visualization::KeyboardEvent &event, void* viewer_void) {
        pcl::visualization::PCLVisualizer *viewer = static_cast<pcl::visualization::PCLVisualizer *> (viewer_void);
        
        if (event.getKeySym() == "w" && event.keyDown()) {
            // Initiate a bounding box
            bb.initCube();
            viewer->removeAllShapes();
            viewer->addLine(bb.p1, bb.p2, "line1");
            viewer->addLine(bb.p2, bb.p3, "line2");
            viewer->addLine(bb.p3, bb.p4, "line3");
            viewer->addLine(bb.p4, bb.p1, "line4");
            viewer->addLine(bb.p1, bb.p5, "line5");
            viewer->addLine(bb.p2, bb.p6, "line6");
            viewer->addLine(bb.p3, bb.p7, "line7");
            viewer->addLine(bb.p4, bb.p8, "line8");
            viewer->addLine(bb.p5, bb.p6, "line9");
            viewer->addLine(bb.p6, bb.p7, "line10");
            viewer->addLine(bb.p7, bb.p8, "line11");
            viewer->addLine(bb.p8, bb.p5, "line12");
        }
        else if (event.getKeySym() == "Right" && event.keyDown()) {
            bb.moveBoxRight();
            viewer->removeAllShapes();
            viewer->addLine(bb.p1, bb.p2, "line1");
            viewer->addLine(bb.p2, bb.p3, "line2");
            viewer->addLine(bb.p3, bb.p4, "line3");
            viewer->addLine(bb.p4, bb.p1, "line4");
            viewer->addLine(bb.p1, bb.p5, "line5");
            viewer->addLine(bb.p2, bb.p6, "line6");
            viewer->addLine(bb.p3, bb.p7, "line7");
            viewer->addLine(bb.p4, bb.p8, "line8");
            viewer->addLine(bb.p5, bb.p6, "line9");
            viewer->addLine(bb.p6, bb.p7, "line10");
            viewer->addLine(bb.p7, bb.p8, "line11");
            viewer->addLine(bb.p8, bb.p5, "line12");
        }
        else if (event.getKeySym() == "Left" && event.keyDown()) {
            bb.moveBoxLeft();
            viewer->removeAllShapes();
            viewer->addLine(bb.p1, bb.p2, "line1");
            viewer->addLine(bb.p2, bb.p3, "line2");
            viewer->addLine(bb.p3, bb.p4, "line3");
            viewer->addLine(bb.p4, bb.p1, "line4");
            viewer->addLine(bb.p1, bb.p5, "line5");
            viewer->addLine(bb.p2, bb.p6, "line6");
            viewer->addLine(bb.p3, bb.p7, "line7");
            viewer->addLine(bb.p4, bb.p8, "line8");
            viewer->addLine(bb.p5, bb.p6, "line9");
            viewer->addLine(bb.p6, bb.p7, "line10");
            viewer->addLine(bb.p7, bb.p8, "line11");
            viewer->addLine(bb.p8, bb.p5, "line12");
        }
        else if (event.getKeySym() == "Up" && event.keyDown()) {
            bb.moveBoxUp();
            viewer->removeAllShapes();
            viewer->addLine(bb.p1, bb.p2, "line1");
            viewer->addLine(bb.p2, bb.p3, "line2");
            viewer->addLine(bb.p3, bb.p4, "line3");
            viewer->addLine(bb.p4, bb.p1, "line4");
            viewer->addLine(bb.p1, bb.p5, "line5");
            viewer->addLine(bb.p2, bb.p6, "line6");
            viewer->addLine(bb.p3, bb.p7, "line7");
            viewer->addLine(bb.p4, bb.p8, "line8");
            viewer->addLine(bb.p5, bb.p6, "line9");
            viewer->addLine(bb.p6, bb.p7, "line10");
            viewer->addLine(bb.p7, bb.p8, "line11");
            viewer->addLine(bb.p8, bb.p5, "line12");
        }
        else if (event.getKeySym() == "Down" && event.keyDown()) {
            bb.moveBoxDown();
            viewer->removeAllShapes();
            viewer->addLine(bb.p1, bb.p2, "line1");
            viewer->addLine(bb.p2, bb.p3, "line2");
            viewer->addLine(bb.p3, bb.p4, "line3");
            viewer->addLine(bb.p4, bb.p1, "line4");
            viewer->addLine(bb.p1, bb.p5, "line5");
            viewer->addLine(bb.p2, bb.p6, "line6");
            viewer->addLine(bb.p3, bb.p7, "line7");
            viewer->addLine(bb.p4, bb.p8, "line8");
            viewer->addLine(bb.p5, bb.p6, "line9");
            viewer->addLine(bb.p6, bb.p7, "line10");
            viewer->addLine(bb.p7, bb.p8, "line11");
            viewer->addLine(bb.p8, bb.p5, "line12");
        }
        else if (event.getKeySym() == "k" && event.keyDown()) {
            bb.cropCloud();
            bb.createSegments();
            bb.polyFit();
            bb.estValues();
            bb.getDist();
            bb.getRauheit();
            bb.printResults();
        }
}

pcl::visualization::PCLVisualizer::Ptr interactionCustomizationVis () {
        visualization::PCLVisualizer::Ptr viewer(new visualization::PCLVisualizer("3D Viewer"));
        viewer->setBackgroundColor (0, 0, 0);
        viewer->addCoordinateSystem (100.0);
        pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ> color (cloud, 0, 255, 0);
        viewer->addPointCloud(cloud, color, "first");
        viewer->registerKeyboardCallback (keyboardEventOccurred, (void*)viewer.get());

        return (viewer);
}

void printDetails(PointCloud<PointXYZ>::Ptr cloud, PointXYZ min, PointXYZ max) {
    cout << "The original cloud has " << cloud->size() << " points." << endl;
    cout << "The width of the cloud (x) is: " << abs(min.x-max.x) << " mm." << endl;
    cout << "The height of the cloud (y) is: " << abs(min.y-max.y) << " mm." << endl;
    cout << "The depth of the cloud (z) is: " << abs(min.z-max.z) << " mm." << endl;

    cout << "The min and max z values are: " << min.z << ", " << max.z << endl;

    cout << "By dividing the width in 4 parts, each part will be " << (abs(min.x-max.x)/4) << " mm." << endl;
    cout << "By dividing the height in 4 parts, each part will be " << (abs(min.y-max.y)/4) << " mm." << endl << endl << endl;
}

int main(int argc, char* argv[]) {
    if(argc < 2) {
        cerr << "interactive_grinding_evaluation: Missing input file!\n";
        cerr << "Usage: ./interactive_grinding_evaluation <pcd input file> [Options]...\n\n";
        cerr << "Options:\n\t-s: saves the selected area of the cloud.\n";
        return EXIT_FAILURE;
    }

    vector<string> args;
    for(int i = 2; i < argc; i++) {
        args.push_back(argv[i]);
    }

    for(const auto arg : args) {
        if(arg == "-s") {
            bb.save_file = true;
        }
    }

    pcl::visualization::PCLVisualizer::Ptr viewer (new pcl::visualization::PCLVisualizer);
    bb.file_path = argv[1];
    PointXYZ mmin;
    PointXYZ mmax;

    pcl::PCDReader reader;
    if(reader.read<pcl::PointXYZ>(bb.file_path, *cloud)<0) {
        cerr << "Error reading the pcd file, make sure your command arguments are correct.\n";
        return EXIT_FAILURE;
    };

    getMinMax3D(*cloud, mmin, mmax);
    printDetails(cloud, mmin, mmax);

    viewer = interactionCustomizationVis();
    while(!viewer->wasStopped()) {
        viewer->spinOnce(100);
        std::this_thread::sleep_for(100ms);
    }
}