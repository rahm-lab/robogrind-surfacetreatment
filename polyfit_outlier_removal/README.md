# outlier removal

This package contains code which detects outliers from point clouds, extracts them and creates a processed pcd-file without outliers and pcd-files with only outliers

## Requirements

- PCL Version 1.10 or newer 

Install the pcl in ubuntu 20.04 with `sudo apt install libpcl-dev`

## Build this directory

To build this directory, follow these steps:

```
cd PATH/TO/YOUR/DIRECTORY/polyfit_outlier_removal
mkdir build
cd build/
cmake ..
make
```

To run the executable:

```
./2d_pcd_curve_fitting [YOUR_PCD_FILE.pcd]
```

## View point clouds

If you want to view your pcd-files, install pcl-tools with `sudo apt-get install -y pcl-tools`.
Then you can use the pcl_viewer to view the pcd:
```
pcl_viewer multiview 1 [YOUR_PCD_FILE.pcd]
```
