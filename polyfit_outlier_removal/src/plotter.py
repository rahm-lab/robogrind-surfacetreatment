import os

import matplotlib.pyplot as plt
import numpy as np


def text_to_list(filename):
    result_list = []
    listoflist = []

    with open(filename, 'r') as file:
        for line in file:
            element = line.strip()
            if element != '' and element != "&":
                result_list.append(float(element))
            elif element == "&":
                listoflist.append(result_list.copy())
                result_list.clear()

    return listoflist


def plot_polynomial_and_dataset(coefficients, dataset_x, dataset_y):
    plt.ion()
    x_polynomial = np.linspace(min(dataset_x[0]), max(dataset_x[0]), 600)
    y_polynomial = np.polyval(coefficients[0], x_polynomial)
    x_dataset = dataset_x[0]
    y_dataset = dataset_y[0]

    fig = plt.figure()
    ax = fig.add_subplot(111)
    line1, = ax.plot(x_polynomial, y_polynomial, 'b-', label="Best-Fit Polynomial", linewidth=1)
    line2, = ax.plot(x_dataset, y_dataset, 'ro', label="Cloud Dataset", markersize=0.4)
    
    plt.title("Polynomial and Dataset Plot #1")
    plt.xlabel("X-axis")
    plt.ylabel("Z-axis")
    plt.grid(True) 
    plt.legend()
    
    for i in range(1, len(coefficients)):
        plt.title(f"Polynomial and Dataset Plot #{i+1}")

        # Calculate and extract the polynomial and x values
        x_polynomial = np.linspace(min(dataset_x[i]), max(dataset_x[i]), 600)
        y_polynomial = np.polyval(coefficients[i], x_polynomial)
        x_dataset = dataset_x[i]
        y_dataset = dataset_y[i]
         
        line1.set_xdata(x_polynomial)
        line1.set_ydata(y_polynomial)
        line2.set_xdata(x_dataset)
        line2.set_ydata(y_dataset)

        fig.canvas.draw()

        fig.canvas.flush_events()

        if plt.waitforbuttonpress(timeout=0.1):
            key = plt.gcf().canvas.key_press_event.key
            if key == 'q':
                break

    plt.ioff()

# get the current working directory
dir = os.path.dirname(__file__)

# Importing polynomial coefficients, x- and z-values
coefficients = text_to_list(dir + "/../data/coefs.txt")
dataset_x = text_to_list(dir + "/../data/x-values.txt")
dataset_z = text_to_list(dir + "/../data/z-values.txt")

# Call the function to plot the polynomial and dataset
plot_polynomial_and_dataset(coefficients, dataset_x, dataset_z)