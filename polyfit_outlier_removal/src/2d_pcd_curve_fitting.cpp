#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <boost/program_options.hpp>
#include <pcl/common/common.h>
#include <pcl/io/pcd_io.h>
#include <Eigen/Dense>
#include <Eigen/QR>
#include <iostream>
#include <chrono>

using namespace boost;
using namespace std;
using namespace pcl;
namespace po = boost::program_options;

// A helper function to simplify the main part.
template<class T>
ostream& operator<<(ostream& os, const vector<T>& v)
{
    copy(v.begin(), v.end(), ostream_iterator<T>(os, " "));
    return os;
}

// Store the extracted x-vectors and z-vectors
struct Vectors {
    vector<vector<double>> xvectors;
    vector<vector<double>> zvectors;
};

// Store all points of the same y-value in one vector. Each vector is then stored in pointvectors.
Vectors createSegments(PointCloud<PointXYZ>::Ptr cloud, bool spin=false);

// Estimate the polynomial for the input dataset
void polyfit(vector<vector<double>> &xvecs,
		     vector<vector<double>> &zvecs,
		     vector<vector<double>> &coeffs,
		     int order);

// Convert and save the input dataset into a text file
void vec2Text(const vector<vector<double>>& data, const string& filename);

// Calculate the values using the estimated polynomial
vector<vector<double>> estValues(vector<vector<double>> polyCoefs, vector<vector<double>> xvectors);

// Calculate the distance between the points of each original dataset and polynomial-estimated dataset along with the averages
pair<vector<vector<double>>, vector<double>> getDist(vector<vector<double>> zvectors, vector<vector<double>> est_zvectors);

// Filter out points with distance above the threshold value
void filterCloud(PointCloud<PointXYZ>::Ptr cloud, PointCloud<PointXYZ>::Ptr filtered_cloud,
                 PointCloud<PointXYZ>::Ptr outliers_cloud, pair<vector<vector<double>>, vector<double>> distances, vector<vector<double>> points, double t_factor = 2.0);

// Remove inaccuratly detected outliers using statistical removal
void cleanCloud(PointCloud<PointXYZ>::Ptr outliers_cloud, PointCloud<PointXYZ>::Ptr clean_cloud, double MeanK, double SDM);

// View the input cloud
void cloudsViewer(PointCloud<PointXYZ>::Ptr &cloud, bool spin = false);
void cloudsViewer(PointCloud<PointXYZ>::Ptr &cloud, PointCloud<PointXYZ>::Ptr &cloud2);

int main(int ac, char* av[])
{
	std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
    
    // Get current directory path
    char dir[256];
    getcwd(dir, 256);

	// Declare supported program_options
	std::string file_path;
	
	po::options_description hidden("Hidden options");
	hidden.add_options()
    	("input-file", po::value< std::string >(&file_path), "input file")
    ;     

	po::options_description cmdline_options;
        cmdline_options.add(hidden);

	po::positional_options_description p;
	p.add("input-file", -1);

	po::variables_map vm;
	po::store(po::command_line_parser(ac, av).
          options(cmdline_options).positional(p).run(), vm);
	po::notify(vm);

	if (vm.count("help")) {
		cout << "Usage: statistica_removal [file_path]\n";
    	return 0;
	}

	// Load the original pcd to a point cloud pointer
    PointCloud<PointXYZ>::Ptr cloud (new PointCloud<PointXYZ>);
    PointCloud<PointXYZ>::Ptr viscloud (new PointCloud<PointXYZ>);
	PCDReader reader;
	reader.read<PointXYZ>(file_path, *cloud);

    // Sort point cloud in y-ascending order
    std::sort(cloud->begin(), cloud->end(), [](const PointXYZ& p1, const PointXYZ& p2) {
        return p1.y < p2.y;
    });
    
    // Extract the x-z segments from the cloud
    Vectors xzvectors;
    xzvectors = createSegments(cloud, false); // set to true to visualize splines iteratively

    // Apply the ploynomial fit on each segment
    vector<vector<double>> polyCoefs;
    int order = 2;
    polyfit(xzvectors.xvectors, xzvectors.zvectors, polyCoefs, order);

    // Save the segments and polynomial coefficients
    vec2Text(xzvectors.xvectors, (string(dir) + "/../data/x-values.txt"));
    vec2Text(xzvectors.zvectors, (string(dir) + "/../data/z-values.txt"));
    vec2Text(polyCoefs, (string(dir) + "/../data/coefs.txt"));

    // Calculate the z-values using the polynomial and the distance between each value and its original correspondence 
    vector<vector<double>> est_zvectors = estValues(polyCoefs, xzvectors.xvectors);
    pair<vector<vector<double>>, vector<double>> sq_distances = getDist(xzvectors.zvectors, est_zvectors);

    // Filter out outliers
    PointCloud<PointXYZ>::Ptr filtered_cloud (new PointCloud<PointXYZ>);
    PointCloud<PointXYZ>::Ptr outliers_cloud (new PointCloud<PointXYZ>);
    double t_factor = 2; // higher factor -> more accurate outlier extraction + smaller outlier cloud (2)
    filterCloud(cloud, filtered_cloud, outliers_cloud, sq_distances, xzvectors.zvectors, t_factor);

    // Create the filtering object 
    //statistical removal parameters
    int MeanK(250); // higher value (>=100) -> smoother removal resutls + bigger outliers cloud (250)
	float SDM(-0.5); // lower value (<=2) -> more precise removal results + smaller outliers cloud (-0.5)
    PointCloud<PointXYZ>::Ptr clean_outliers_cloud (new PointCloud<PointXYZ>);
	cleanCloud(outliers_cloud, clean_outliers_cloud, MeanK, SDM);
	std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
	std::cout << "\nProcess time = " << std::chrono::duration_cast<std::chrono::seconds>(end - begin).count() << " s" << std::endl;

    cloudsViewer(cloud); // view original cloud
    cloudsViewer(filtered_cloud); // view cloud without outliers (damage areas)
    cloudsViewer(cloud, outliers_cloud); // view outliers only
    //cloudsViewer(clean_outliers_cloud); // view outliers only after statistical removal
    cloudsViewer(cloud, clean_outliers_cloud); // view outliers only after statistical removal

	end = std::chrono::steady_clock::now();
	std::cout << "\nRun time = " << std::chrono::duration_cast<std::chrono::seconds>(end - begin).count() << " s" << std::endl;
    return 0;
}

Vectors createSegments(PointCloud<PointXYZ>::Ptr cloud, bool spin) {
    Vectors rvecs;
    // Declare assisting variables
    float y0 = cloud->points[0].y;
    vector<PointXYZ> pv;
    vector<double> xvalues, zvalues;
    vector<vector<PointXYZ>> pointvectors;
    vector<vector<double>> xvectors, zvectors;
    int vec_size = 0;
    int vec_num = 0;
    size_t extracted_points = 0;

    if(spin == true) {
        PointCloud<PointXYZ>::Ptr viscloud (new PointCloud<PointXYZ>); // create cloud for iterative view
        for(const auto& point : *cloud) {    
            if(point.y == y0) {
                pv.push_back(point);
                viscloud->push_back(point); // append spline point
                xvalues.push_back(point.x);
                zvalues.push_back(point.z);
                extracted_points++;
            }
            else {
                pointvectors.push_back(pv);
                xvectors.push_back(xvalues);
                zvectors.push_back(zvalues);
                vec_size += pv.size();
                cloudsViewer(viscloud, true); // view the extracted spline
                viscloud->clear(); // reset the iterative cloud
                vec_num++;
                extracted_points++;
                y0 = point.y;
                pv.clear();
                xvalues.clear();
                zvalues.clear();
                pv.push_back(point);
                xvalues.push_back(point.x);
                zvalues.push_back(point.z);
            }
        }
    } else {
        for(const auto& point : *cloud) {    
            if(point.y == y0) {
                pv.push_back(point);
                xvalues.push_back(point.x);
                zvalues.push_back(point.z);
                extracted_points++;
            }
            else {
                pointvectors.push_back(pv);
                xvectors.push_back(xvalues);
                zvectors.push_back(zvalues);
                vec_size += pv.size();
                vec_num++;
                extracted_points++;
                y0 = point.y;
                pv.clear();
                xvalues.clear();
                zvalues.clear();
                pv.push_back(point);
                xvalues.push_back(point.x);
                zvalues.push_back(point.z);
            }
        }
    }

    // Process the last segment of points
    pointvectors.push_back(pv);
    xvectors.push_back(xvalues);
    zvectors.push_back(zvalues);
    vec_size += pv.size();
    vec_num++;
    extracted_points++;

    // print processig element features
    float avgsize = vec_size / vec_num;
    printf("The cloud has %lu points.\n", cloud->points.size());
    printf("The program has extracted %lu points.\n", extracted_points);
    printf("The extracted points are split into %lu z-parts, and %lu x-parts.\n", zvectors.size(), xvectors.size());
    printf("Each part has, on average, %f elements\n\n", avgsize);

    rvecs.xvectors = xvectors;
    rvecs.zvectors = zvectors;
    return rvecs;
}

void polyfit(vector<vector<double>> &xvecs,
		     vector<vector<double>> &zvecs,
		     vector<vector<double>> &coeffs,
		     int order) {
    vector<double> coeff;

    for(int i = 0; i < xvecs.size(); i++) {
        if(!(xvecs[i].size() >= order + 1)) {
            xvecs.erase(xvecs.begin() + i);
            zvecs.erase(zvecs.begin() + i);
            i--;
            continue;
        }

        // Create Matrix Placeholder of size n x k, n= number of datapoints, k = order of polynomial, for exame k = 3 for cubic polynomial
        Eigen::MatrixXd T(xvecs[i].size(), order + 1);
        Eigen::VectorXd V = Eigen::VectorXd::Map(&zvecs[i].front(), zvecs[i].size());
        Eigen::VectorXd result;

        // check to make sure inputs are correct
        assert(xvecs[i].size() == zvecs[i].size());
        assert(xvecs[i].size() >= order + 1);

        // Populate the matrix
        for(size_t j = 0 ; j < xvecs[i].size(); ++j)
        {
            for(size_t k = 0; k < order + 1; ++k)
            {
                T(j, k) = pow(xvecs[i].at(j), k);
            }
        }
        
        // Solve for linear least square fit
        result  = T.householderQr().solve(V);
        coeff.resize(order+1);
        for (int n = 0; n < order+1; n++)
        {
            coeff[n] = result[n];
        }

        std::reverse(coeff.begin(), coeff.end());
        coeffs.push_back(coeff);
        coeff.clear();
    }
}

void vec2Text(const vector<vector<double>>& data, const string& filename) {
    std::ofstream outputFile(filename);

    if (outputFile.is_open()) {
        for (const vector<double>& innerVec : data) {
            for (double value : innerVec) {
                outputFile << value << '\n';
            }
            outputFile << '\n'; // Empty line after each inner vector
            outputFile << "&\n"; // Marking separator
            outputFile << '\n'; // Empty line after the separator
        }
        outputFile.close();
        cout << "Nested Vector stored in file: " << filename << endl;
    } else {
        cout << "Unable to open the file: " << filename << endl;
    }
}

vector<vector<double>> estValues(vector<vector<double>> polyCoefs, vector<vector<double>> xvectors) {
    int vcounter(0);
    double zEst;
    vector<double> estZVec;
    vector<vector<double>> est_zvectors;
    for(vector<double> vector : xvectors) {
        for(double x : vector) {
            //zEst = ((pow(x,3))*polyCoefs[vcounter][0]) + ((pow(x,2))*polyCoefs[vcounter][1]) + (x*polyCoefs[vcounter][2]) + polyCoefs[vcounter][3]; // 3rd degree poly
            zEst = ((pow(x,2))*polyCoefs[vcounter][0]) + (x*polyCoefs[vcounter][1]) + polyCoefs[vcounter][2]; // 2nd degree poly
            //zEst =  (x*polyCoefs[vcounter][0]) + polyCoefs[vcounter][1]; // 1st degree poly
            estZVec.push_back(zEst);
        }
        est_zvectors.push_back(estZVec);
        estZVec.clear();
        vcounter++;
    }
    return est_zvectors;
}

pair<vector<vector<double>>, vector<double>> getDist(vector<vector<double>> zvectors, vector<vector<double>> est_zvectors) {
    double dist, total, avg_dist;
    vector<double> distances;
    vector<vector<double>> distance_vectors;
    vector<double> avg_distances;
    pair<vector<vector<double>>, vector<double>> return_pair;

    for(int i = 0; i < zvectors.size(); i++) {
        for(int j = 0; j < zvectors[i].size(); j++) {
            dist = zvectors[i][j] - est_zvectors[i][j];
            dist = pow(dist, 2);
            distances.push_back(dist);
            total += dist;
        }
        distance_vectors.push_back(distances);
        avg_dist = total / distances.size();
        avg_distances.push_back(avg_dist);
        total = 0;
        distances.clear();
    }

    return_pair.first = distance_vectors;
    return_pair.second = avg_distances;

    return return_pair;
}

void filterCloud(PointCloud<PointXYZ>::Ptr cloud, PointCloud<PointXYZ>::Ptr filtered_cloud,
                 PointCloud<PointXYZ>::Ptr outliers_cloud, pair<vector<vector<double>>, vector<double>> dist_pair, vector<vector<double>> points, double t_factor) {
                    int pcounter(0);
                    int acounter(0);

                    vector<double> pointValues;
                    // Accumulate the elements from points into pointValues using std::accumulate
                    pointValues = accumulate(points.begin(), points.end(), vector<double>(),
                        [](std::vector<double>& acc, const std::vector<double>& innerVector) {
                            acc.insert(acc.end(), innerVector.begin(), innerVector.end());
                            return acc;
                        });
                    
                    for(vector<double> distances : dist_pair.first) {
                        for(double dist : distances) {
                            if(dist < (dist_pair.second[acounter] + (dist_pair.second[acounter]*t_factor))) {
                            // point is an inlier
                            filtered_cloud->push_back(cloud->points[pcounter]);
                            }
                            else {
                            // point is an outlier
                            outliers_cloud->push_back(cloud->points[pcounter]);
                            }
                            pcounter++;
                        }
                        acounter++;
                    }

                    printf("\nThe filtered cloud has %lu points.\n", filtered_cloud->points.size());
                    printf("The outliers cloud has %lu points.\n", outliers_cloud->points.size());
                }

void cleanCloud(PointCloud<PointXYZ>::Ptr outliers_cloud, PointCloud<PointXYZ>::Ptr clean_cloud, double MeanK, double SDM) {
    pcl::StatisticalOutlierRemoval<pcl::PointXYZ> sor;
	sor.setInputCloud(outliers_cloud);
	sor.setMeanK(MeanK);								//MeanK should be around 500 with pcd-files >2 million points
	sor.setStddevMulThresh(SDM);						//Segment areas are bigger with lower multiplier values
	sor.filter(*clean_cloud);

    printf("The statistical outliers cloud has %lu points.\n", clean_cloud->points.size());
}

void cloudsViewer(PointCloud<PointXYZ>::Ptr &cloud, bool spin) {
    visualization::PCLVisualizer::Ptr viewer(new visualization::PCLVisualizer("3D Viewer"));
    visualization::PointCloudColorHandlerCustom<PointXYZ> source_color(cloud, 0, 255, 0);
    viewer->addPointCloud<PointXYZ>(cloud, source_color, "first");
    viewer->addCoordinateSystem(100.0,"cloud"); // x=red y=green z=blue
    if(spin == false)
        viewer->spin();
    else {
        viewer->setCameraPosition(0, -4000, 0, 0, 5, -1, 0, 0, 0);
        viewer->spinOnce(1);
    }
}

void cloudsViewer(PointCloud<PointXYZ>::Ptr &cloud, PointCloud<PointXYZ>::Ptr &cloud2) {
    visualization::PCLVisualizer::Ptr viewer(new visualization::PCLVisualizer("3D Viewer"));
    visualization::PointCloudColorHandlerCustom<PointXYZ> source_color(cloud, 0, 255, 0);
    visualization::PointCloudColorHandlerCustom<PointXYZ> outlier_color(cloud2, 255, 0, 0);
    viewer->addPointCloud<PointXYZ>(cloud, source_color, "first");
    viewer->addPointCloud<PointXYZ>(cloud2, outlier_color, "second");
    viewer->addCoordinateSystem(100.0,"cloud"); // x=red y=green z=blue
    viewer->spin();
}